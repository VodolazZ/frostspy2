﻿namespace FrostSpy2
{
    using System.Linq;
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Drawing;
    using System.Net;
    using System.Net.Http;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows.Forms;

    public partial class FrostSpy : Form
    {
        private const string FILENAME_CONFIG = "config.xml";
        private const int MIN_LEFT_RIGHT_BORDER = 15;
        private const int MIN_UP_BOTTOM_BORDER = 10;

        private static bool expandedReportEnabled;
        
        private static List<string> chosenGames;
        private static List<Button> gamesButtons;
        private static List<Button> environmentButtons;
        private static string chosenEnv;

        private static HttpClient client = new HttpClient();
        private static string lastSuccessfulConnection;
        private static Component currentlyCheckedComponents;

        public FrostSpy()
        {
            this.InitializeComponent();
            chosenGames = new List<string>();

            ConfigParser.Parse(FILENAME_CONFIG);
            this.InitializeEnvironmentButtons(ConfigParser.GetParsedEnvironments.Keys.ToList());
            this.InitializeGamesButtons(ConfigParser.GetParsedGames);
            this.InitializeBitChecker();
            currentlyCheckedComponents = Component.NoComponent;

            this.Text = "FrostSpy 2";
        }

        private void InitializeEnvironmentButtons(List<string> environments)
        {
            if (environments.Count > 8 || environments.Count < 1)
                throw new ArgumentException($"app supports from 1 to 8 environments! Given: {environments.Count}!");

            environmentButtons = new List<Button>();
            int envsCount = 0;
            int buttonWidth = 105;
            int buttonHeight = 40;

            foreach (var env in environments)
            {
                int horizontalOffset = envsCount == 0 ?
                    MIN_LEFT_RIGHT_BORDER + buttonWidth: 
                    MIN_LEFT_RIGHT_BORDER + buttonWidth + envsCount * buttonWidth;
                int verticalOffset = MIN_UP_BOTTOM_BORDER;

                Button btn = new Button();
                btn.Text = env;
                btn.Name = env;
                btn.Size = new Size()
                {
                    Height = buttonHeight,
                    Width = buttonWidth
                };

                btn.Location = new Point()
                {
                    X = horizontalOffset,
                    Y = verticalOffset
                };

                this.Controls.Add(btn);

                if (envsCount == 0)
                {
                    btn.Enabled = false;
                    btn.BackColor = Color.Brown;
                    btn.ForeColor = Color.Black;
                    chosenEnv = env;
                }

                btn.Click += this.EnvironmentChangeHandler;
                environmentButtons.Add(btn);
                envsCount++;
            }
        }

        private void InitializeGamesButtons(List<Game> games)
        {
            if (games.Count > 21)
                throw new ArgumentException("application supports only max 21 games!");
            else if (games.Count <= 0)
                throw new ArgumentException("Config file should contain at least 1 game");

            gamesButtons = new List<Button>();

            int ButtonBlockYOffset = 370;
            int gamesCount = 0;
            int gamesInAColumn = 3;
            int buttonWidth = 95;
            int buttonHeight = 30;

            foreach (var game in games)
            {
                int horizontalOffset = MIN_LEFT_RIGHT_BORDER + gamesCount / gamesInAColumn * (buttonWidth + MIN_LEFT_RIGHT_BORDER);
                int verticalOffset = MIN_UP_BOTTOM_BORDER + gamesCount % gamesInAColumn * (buttonHeight + MIN_UP_BOTTOM_BORDER) + ButtonBlockYOffset;

                Button btn = new Button();
                btn.Text = game.Name;
                btn.Name = game.Name;
                btn.Size = new Size()
                {
                    Height = buttonHeight,
                    Width = buttonWidth                   
                };

                btn.Location = new Point()
                {
                    X = horizontalOffset,
                    Y = verticalOffset
                };

                btn.Click += this.GameButtonClickHandler;
                this.Controls.Add(btn);

                btn.Refresh();
                gamesButtons.Add(btn);

                gamesCount++;
            }
        }

        private void InitializeBitChecker()
        {
            this.bitCheckboxX32.Checked = true;
            this.bitCheckboxX64.Checked = false;

            QueryResultParser.get32BitComponents = this.bitCheckboxX32.Checked;
            QueryResultParser.get64BitComponents = this.bitCheckboxX64.Checked;
        }

        private void GameButtonClickHandler(object sender, EventArgs ea)
        {
            var button = sender as Button;
            if (button == null)
                throw new ArgumentException("Non-button object invokes game button click handler!");

            if (chosenGames.Contains(button.Name))
            {
                chosenGames.Remove(button.Name);
                ChangeButtonView(button, false);

                if (this.ChooseAll.BackColor == Color.Brown)
                {
                    ChangeButtonView(this.ChooseAll, false);
                }
            }
            else
            {
                chosenGames.Add(button.Name);
                ChangeButtonView(button, true);

                if (chosenGames.Count == gamesButtons.Count)
                {
                    ChangeButtonView(this.ChooseAll, true);
                }
            }
        }

        private void EnvironmentChangeHandler(object sender, EventArgs ea)
        {
            Button btn = sender as Button;
            if (btn == null)
            {
                throw new ArgumentException("Wrong initiator triggerred environment change!");
            }

            foreach (Button button in environmentButtons)
            {
                button.Enabled = true;
                ChangeButtonView(button, false);
            }

            btn.Enabled = false;
            ChangeButtonView(btn, true);

            chosenEnv = btn.Name;
            if (expandedReportEnabled)
            {
                this.MainInfoWindow.AppendText($"{chosenEnv} environment chosen." + Environment.NewLine);
            }
        }

        private async void RequestButtonClickHandler(object sender, EventArgs ea)
        {
            var button = sender as Button;
            if (button == null)
                throw new ArgumentException("Non-button object invokes game button click handler!");
            if (chosenGames.Count == 0)
            {
                this.MainInfoWindow.AppendText("No games chosen, choose game!" + Environment.NewLine + Environment.NewLine);
                return;
            }
            if (currentlyCheckedComponents == Component.NoComponent)
            {
                this.MainInfoWindow.AppendText("No component chosen, choose component!" + Environment.NewLine + Environment.NewLine);
                return;
            }
            
            this.StartWaitingResponse();

            if (expandedReportEnabled)
            {
                this.MainInfoWindow.AppendText("Started data request for games:" + Environment.NewLine);
                foreach (string game in chosenGames)
                {
                    this.MainInfoWindow.AppendText(game);
                    this.MainInfoWindow.AppendText(Environment.NewLine);
                }
            }

            var requestedGames = ConfigParser.GetParsedGames.Where(game => chosenGames.Contains(game.Name));

            try
            {
                foreach (Game game in requestedGames)
                {
                    if (expandedReportEnabled)
                    {
                        this.MainInfoWindow.AppendText($"Started request pipeline for {game.Name}..." + Environment.NewLine);
                    }

                    bool isOverridenTemplate = false;
                    string template;
                    if (game.OverrideRules.ContainsKey(chosenEnv))
                    {
                        template = game.OverrideRules[chosenEnv];
                        isOverridenTemplate = true;
                    }
                    else
                    {
                        template = ConfigParser.GetParsedEnvironments[chosenEnv];
                    }

                    if (string.IsNullOrEmpty(template))
                        throw new DataException($"no connection string found for {game.Name} in {chosenEnv}!");
                    
                    StringBuilder sb = new StringBuilder(template);
                    foreach (var param in game.ParamsCollection)
                    {
                        sb.Replace($"{{{param.Key}}}", param.Value);
                    }

                    string connectionString = string.Empty;
                    if (!isOverridenTemplate)
                    {
                        connectionString = await this.ChooseVitalConnectionAsync(sb);
                    }
                    sb.Replace("{connectionString}", connectionString);

                    if (expandedReportEnabled)
                    {
                        this.MainInfoWindow.AppendText($"Prepared connection uri for game {game.Name}: {sb}" + Environment.NewLine);
                    }

                    string result = await this.QueryHandler(sb.ToString());

                    if (!string.IsNullOrEmpty(result))
                    {
                        result = QueryResultParser.Parse(result, currentlyCheckedComponents);
                        this.MainInfoWindow.AppendText($"Results for {game.Name}:" + Environment.NewLine);
                        this.MainInfoWindow.AppendText(result + Environment.NewLine);
                    }
                    else
                    {
                        this.MainInfoWindow.AppendText($"Web request for {game.Name} from {template} failed! No result.");
                    }
                }
            }
            catch (Exception e)
            {
                this.MainInfoWindow.AppendText(Environment.NewLine);
                this.MainInfoWindow.AppendText(e.Message);
                this.MainInfoWindow.AppendText(Environment.NewLine);
                this.StopWaitingResponse();
            }

            this.StopWaitingResponse();
        }

        private async Task<string> QueryHandler(string addr)
        {
            try
            {
                var result = await client.GetStringAsync(addr);
                return result;
            }
            catch (WebException ex)
            {
                return ex.Message;
            }
        }

        private async Task<string> ChooseVitalConnectionAsync(StringBuilder preparedTemplate)
        {
            //the following empty catch block is damn bad 
            //but I can't check http resourse w/o raising error when server itself is not found
            try
            {
                string uri = preparedTemplate.ToString().Replace("{connectionString}", lastSuccessfulConnection);
                var result = await client.GetAsync(uri);

                if (result.StatusCode == HttpStatusCode.OK)
                {
                    return lastSuccessfulConnection;
                }
            }
            catch (Exception)
            {
            }

            foreach (string connectionStr in ConfigParser.GetParsedConnections)
            {
                string uri = preparedTemplate.ToString().Replace("{connectionString}", connectionStr);
                try
                {
                    var result = await client.GetAsync(uri);
                    if (result.StatusCode == HttpStatusCode.OK)
                    {
                        lastSuccessfulConnection = connectionStr;
                        return connectionStr;
                    }
                }
                catch (Exception)
                {
                    continue;
                }
            }

            throw new DataException($"No vital connection found for game!");
        }

        private void ChangeExpandedReportSettings(object sender, EventArgs ea)
        {
            var chkBox = sender as CheckBox;
            if (chkBox != null)
            {
                expandedReportEnabled = !expandedReportEnabled;
            }
        }

        private void ComponentChecklistChange(object sender, EventArgs ea)
        {
            var senderAsCheckbox = sender as CheckBox;
            if (senderAsCheckbox == null)
            {
                throw new ArgumentException("Wrong initiator triggerred component change!");
            }

            if (senderAsCheckbox.Name == "All")
            {
                if (senderAsCheckbox.Checked)
                {
                    this.GameShield.Checked = true;
                    this.Driver.Checked = true;
                    this.Launcher.Checked = true;
                    this.Collector.Checked = true;
                    this.Updater.Checked = true;

                    currentlyCheckedComponents = Component.AllGames;
                }
                else
                {
                    this.GameShield.Checked = false;
                    this.Driver.Checked = false;
                    this.Launcher.Checked = false;
                    this.Collector.Checked = false;
                    this.Updater.Checked = false;

                    currentlyCheckedComponents = Component.NoComponent;
                }

                return;
            }

            Component selectedComponent = (Component)Enum.Parse(typeof(Component), senderAsCheckbox.Name);
            if (senderAsCheckbox.Checked)
            {
                currentlyCheckedComponents |= selectedComponent;
            }
            else
            {
                currentlyCheckedComponents ^= selectedComponent;

                if (this.All.Checked)
                {
                    this.All.Checked = false;
                }
            }
        }

        private static void ChangeButtonView(Button bt, bool isSelected)
        {
            if (isSelected)
            {
                bt.BackColor = Color.Brown;
            }
            else
            {
                bt.BackColor = Control.DefaultBackColor;
                bt.UseVisualStyleBackColor = true;
            }
        }

        private void ChooseAllClickHandler(object sender, EventArgs e)
        {
            var button = sender as Button;
            if (button == null)
                throw new ArgumentException("Non-button object invokes game button click handler!");

            if (button.BackColor == Control.DefaultBackColor)
            {
                foreach (Button gamesButton in gamesButtons)
                {
                    ChangeButtonView(gamesButton, true);
                    if (!chosenGames.Contains(gamesButton.Name))
                        chosenGames.Add(gamesButton.Name);
                }
                ChangeButtonView(button, true);
            }
            else
            {
                foreach (Button gamesButton in gamesButtons)
                {
                    ChangeButtonView(gamesButton, false);
                    chosenGames.Clear();
                }

                ChangeButtonView(button, false);
            }
        }

        private void StartWaitingResponse()
        {
            this.Cursor = Cursors.WaitCursor;

            foreach (var gamesButton in gamesButtons)
            {
                gamesButton.Enabled = false;
            }

            foreach (var environmentButton in environmentButtons)
            {
                environmentButton.Enabled = false;
            }

            this.GameShield.Enabled = false;
            this.Driver.Enabled = false;
            this.Launcher.Enabled = false;
            this.Collector.Enabled = false;
            this.Updater.Enabled = false;

            this.ChooseAll.Enabled = false;

            this.RequestProgressBar.Visible = true;
            this.RequestProgressBar.Enabled = true;
        }

        private void StopWaitingResponse()
        {
            foreach (var gamesButton in gamesButtons)
            {
                gamesButton.Enabled = true;
            }

            foreach (var environmentButton in environmentButtons)
            {
                environmentButton.Enabled = true;
            }

            this.GameShield.Enabled = true;
            this.Driver.Enabled = true;
            this.Launcher.Enabled = true;
            this.Collector.Enabled = true;
            this.Updater.Enabled = true;

            this.ChooseAll.Enabled = true;

            this.RequestProgressBar.Visible = false;
            this.RequestProgressBar.Enabled = false;

            this.Cursor = this.DefaultCursor;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.MainInfoWindow.Clear();
        }

        private void BitCheckboxClickHandler(object sender, EventArgs e)
        {
            var checkbox = sender as CheckBox;
            if (checkbox == null)
                throw new ArgumentException("Non-checkbox object invokes game bit checkbox click handler!");

            QueryResultParser.get32BitComponents = this.bitCheckboxX32.Checked;
            QueryResultParser.get64BitComponents = this.bitCheckboxX64.Checked;

            if (QueryResultParser.get32BitComponents | QueryResultParser.get64BitComponents == false)
            {
                QueryResultParser.get32BitComponents = true;
                this.bitCheckboxX32.Checked = true;
            }
        }
    }
}
