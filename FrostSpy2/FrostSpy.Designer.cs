﻿namespace FrostSpy2
{
    partial class FrostSpy
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.MainInfoWindow = new System.Windows.Forms.RichTextBox();
            this.GameShield = new System.Windows.Forms.CheckBox();
            this.Driver = new System.Windows.Forms.CheckBox();
            this.Launcher = new System.Windows.Forms.CheckBox();
            this.Collector = new System.Windows.Forms.CheckBox();
            this.Updater = new System.Windows.Forms.CheckBox();
            this.All = new System.Windows.Forms.CheckBox();
            this.button1 = new System.Windows.Forms.Button();
            this.checkBox8 = new System.Windows.Forms.CheckBox();
            this.ChooseAll = new System.Windows.Forms.Button();
            this.RequestProgressBar = new System.Windows.Forms.ProgressBar();
            this.button2 = new System.Windows.Forms.Button();
            this.bitCheckboxX32 = new System.Windows.Forms.CheckBox();
            this.bitCheckboxX64 = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // MainInfoWindow
            // 
            this.MainInfoWindow.Cursor = System.Windows.Forms.Cursors.Default;
            this.MainInfoWindow.HideSelection = false;
            this.MainInfoWindow.Location = new System.Drawing.Point(12, 54);
            this.MainInfoWindow.Name = "MainInfoWindow";
            this.MainInfoWindow.ReadOnly = true;
            this.MainInfoWindow.Size = new System.Drawing.Size(520, 315);
            this.MainInfoWindow.TabIndex = 0;
            this.MainInfoWindow.TabStop = false;
            this.MainInfoWindow.Text = "";
            // 
            // GameShield
            // 
            this.GameShield.AccessibleName = "";
            this.GameShield.AutoSize = true;
            this.GameShield.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.GameShield.Location = new System.Drawing.Point(579, 103);
            this.GameShield.Name = "GameShield";
            this.GameShield.Size = new System.Drawing.Size(145, 29);
            this.GameShield.TabIndex = 1;
            this.GameShield.Text = "Gameshield";
            this.GameShield.UseVisualStyleBackColor = true;
            this.GameShield.Click += new System.EventHandler(this.ComponentChecklistChange);
            // 
            // Driver
            // 
            this.Driver.AccessibleName = "";
            this.Driver.AutoSize = true;
            this.Driver.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Driver.Location = new System.Drawing.Point(579, 245);
            this.Driver.Name = "Driver";
            this.Driver.Size = new System.Drawing.Size(88, 29);
            this.Driver.TabIndex = 2;
            this.Driver.Text = "Driver";
            this.Driver.UseVisualStyleBackColor = true;
            this.Driver.Click += new System.EventHandler(this.ComponentChecklistChange);
            // 
            // Launcher
            // 
            this.Launcher.AccessibleName = "Launcher";
            this.Launcher.AutoSize = true;
            this.Launcher.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Launcher.Location = new System.Drawing.Point(579, 138);
            this.Launcher.Name = "Launcher";
            this.Launcher.Size = new System.Drawing.Size(121, 29);
            this.Launcher.TabIndex = 3;
            this.Launcher.Text = "Launcher";
            this.Launcher.UseVisualStyleBackColor = true;
            this.Launcher.Click += new System.EventHandler(this.ComponentChecklistChange);
            // 
            // Collector
            // 
            this.Collector.AccessibleName = "Collector";
            this.Collector.AutoSize = true;
            this.Collector.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Collector.Location = new System.Drawing.Point(579, 210);
            this.Collector.Name = "Collector";
            this.Collector.Size = new System.Drawing.Size(116, 29);
            this.Collector.TabIndex = 4;
            this.Collector.Text = "Collector";
            this.Collector.UseVisualStyleBackColor = true;
            this.Collector.Click += new System.EventHandler(this.ComponentChecklistChange);
            // 
            // Updater
            // 
            this.Updater.AccessibleName = "";
            this.Updater.AutoSize = true;
            this.Updater.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Updater.Location = new System.Drawing.Point(579, 175);
            this.Updater.Name = "Updater";
            this.Updater.Size = new System.Drawing.Size(107, 29);
            this.Updater.TabIndex = 5;
            this.Updater.Text = "Updater";
            this.Updater.UseVisualStyleBackColor = true;
            this.Updater.Click += new System.EventHandler(this.ComponentChecklistChange);
            // 
            // All
            // 
            this.All.AccessibleName = "";
            this.All.AutoSize = true;
            this.All.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.All.Location = new System.Drawing.Point(579, 54);
            this.All.Name = "All";
            this.All.Size = new System.Drawing.Size(55, 29);
            this.All.TabIndex = 6;
            this.All.Text = "All";
            this.All.UseVisualStyleBackColor = true;
            this.All.Click += new System.EventHandler(this.ComponentChecklistChange);
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button1.Location = new System.Drawing.Point(579, 296);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(293, 50);
            this.button1.TabIndex = 13;
            this.button1.Text = "Request!";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.RequestButtonClickHandler);
            // 
            // checkBox8
            // 
            this.checkBox8.AutoSize = true;
            this.checkBox8.Location = new System.Drawing.Point(670, 352);
            this.checkBox8.Name = "checkBox8";
            this.checkBox8.Size = new System.Drawing.Size(104, 17);
            this.checkBox8.TabIndex = 14;
            this.checkBox8.Text = "Expanded report";
            this.checkBox8.UseVisualStyleBackColor = true;
            this.checkBox8.Click += new System.EventHandler(this.ChangeExpandedReportSettings);
            // 
            // ChooseAll
            // 
            this.ChooseAll.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ChooseAll.Location = new System.Drawing.Point(796, 403);
            this.ChooseAll.Name = "ChooseAll";
            this.ChooseAll.Size = new System.Drawing.Size(105, 69);
            this.ChooseAll.TabIndex = 15;
            this.ChooseAll.Text = "Choose All";
            this.ChooseAll.UseVisualStyleBackColor = true;
            this.ChooseAll.Click += new System.EventHandler(this.ChooseAllClickHandler);
            // 
            // RequestProgressBar
            // 
            this.RequestProgressBar.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.RequestProgressBar.Enabled = false;
            this.RequestProgressBar.ForeColor = System.Drawing.Color.ForestGreen;
            this.RequestProgressBar.Location = new System.Drawing.Point(79, 175);
            this.RequestProgressBar.MarqueeAnimationSpeed = 8;
            this.RequestProgressBar.Name = "RequestProgressBar";
            this.RequestProgressBar.Size = new System.Drawing.Size(367, 24);
            this.RequestProgressBar.Step = 20;
            this.RequestProgressBar.Style = System.Windows.Forms.ProgressBarStyle.Marquee;
            this.RequestProgressBar.TabIndex = 16;
            this.RequestProgressBar.UseWaitCursor = true;
            this.RequestProgressBar.Visible = false;
            // 
            // button2
            // 
            this.button2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button2.Location = new System.Drawing.Point(12, 12);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(58, 36);
            this.button2.TabIndex = 17;
            this.button2.Text = "Clear";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // bitCheckboxX32
            // 
            this.bitCheckboxX32.AccessibleName = "";
            this.bitCheckboxX32.AutoSize = true;
            this.bitCheckboxX32.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.bitCheckboxX32.Location = new System.Drawing.Point(796, 138);
            this.bitCheckboxX32.Name = "bitCheckboxX32";
            this.bitCheckboxX32.Size = new System.Drawing.Size(66, 29);
            this.bitCheckboxX32.TabIndex = 18;
            this.bitCheckboxX32.Text = "x32";
            this.bitCheckboxX32.UseVisualStyleBackColor = true;
            this.bitCheckboxX32.Click += new System.EventHandler(this.BitCheckboxClickHandler);
            // 
            // bitCheckboxX64
            // 
            this.bitCheckboxX64.AccessibleName = "";
            this.bitCheckboxX64.AutoSize = true;
            this.bitCheckboxX64.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.bitCheckboxX64.Location = new System.Drawing.Point(796, 173);
            this.bitCheckboxX64.Name = "bitCheckboxX64";
            this.bitCheckboxX64.Size = new System.Drawing.Size(66, 29);
            this.bitCheckboxX64.TabIndex = 19;
            this.bitCheckboxX64.Text = "x64";
            this.bitCheckboxX64.UseVisualStyleBackColor = true;
            this.bitCheckboxX64.Click += new System.EventHandler(this.BitCheckboxClickHandler);
            // 
            // FrostSpy
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(913, 505);
            this.Controls.Add(this.bitCheckboxX64);
            this.Controls.Add(this.bitCheckboxX32);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.RequestProgressBar);
            this.Controls.Add(this.ChooseAll);
            this.Controls.Add(this.checkBox8);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.All);
            this.Controls.Add(this.Updater);
            this.Controls.Add(this.Collector);
            this.Controls.Add(this.Launcher);
            this.Controls.Add(this.Driver);
            this.Controls.Add(this.GameShield);
            this.Controls.Add(this.MainInfoWindow);
            this.Name = "FrostSpy";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RichTextBox MainInfoWindow;
        private System.Windows.Forms.CheckBox GameShield;
        private System.Windows.Forms.CheckBox Driver;
        private System.Windows.Forms.CheckBox Launcher;
        private System.Windows.Forms.CheckBox Collector;
        private System.Windows.Forms.CheckBox Updater;
        private System.Windows.Forms.CheckBox All;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.CheckBox checkBox8;
        private System.Windows.Forms.Button ChooseAll;
        private System.Windows.Forms.ProgressBar RequestProgressBar;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.CheckBox bitCheckboxX32;
        private System.Windows.Forms.CheckBox bitCheckboxX64;
    }
}

