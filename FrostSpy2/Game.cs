﻿namespace FrostSpy2
{
    using System.Collections.Generic;

    public class Game
    {
        public string Name { get; set; }

        public readonly Dictionary<string, string> ParamsCollection = new Dictionary<string, string>();
        public readonly Dictionary<string, string> OverrideRules = new Dictionary<string, string>();
    }
}