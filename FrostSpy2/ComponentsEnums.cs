﻿namespace FrostSpy2
{
    using System;

    [Flags]
    public enum Component : byte
    {
        NoComponent = 0,
        GameShield = 2,
        Driver = 4,
        Launcher = 8,
        Collector = 16,
        Updater = 32,
        AllGames = GameShield | Driver | Launcher | Collector | Updater
    }
}
