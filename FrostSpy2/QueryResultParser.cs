﻿namespace FrostSpy2
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Text.RegularExpressions;

    /// <summary>
    /// todo: this class is the first candidate for implementing agile settings. everything should be read from xml file, maybe in the next version?
    /// </summary>
    public static class QueryResultParser
    {
        private const char PARAMS_DELIM = '#';
        private static List<Component> valuableComponents;
        public static bool get32BitComponents { get; set; }
        public static bool get64BitComponents { get; set; }

        static QueryResultParser()
        {
            var collection = Enum.GetNames(typeof(Component)).Where(str => str != "NoComponent" && str != "AllGames");
            valuableComponents = new List<Component>();

            foreach (string compName in collection)
            {
                Component component = (Component)Enum.Parse(typeof(Component), compName);
                valuableComponents.Add(component);
            }
        }


        private static Dictionary<Component, string> componentPatterns32 = new Dictionary<Component, string>
        {
            { Component.Collector, @"# collector32 version:((\d){1,2}|(\.)){7}\|((\d){1,2}|(\.)){7}"},
            { Component.Driver, @"# driver32 version:((\d){1,2}|(\.)){7}\|((\d){1,2}|(\.)){7}"},
            { Component.GameShield, @"# gameshield32 version:((\d){1,2}|(\.)){7}\|((\d){1,2}|(\.)){7}"},
            { Component.Launcher, @"# launcher32 version:((\d){1,2}|(\.)){7}\|((\d){1,2}|(\.)){7}" },
            { Component.Updater, @"# frostupdater32 version:((\d){1,2}|(\.)){7}\|((\d){1,2}|(\.)){7}" }
        };

        private static Dictionary<Component, string> componentPatterns64 = new Dictionary<Component, string>
        {
            { Component.Collector, @"# collector64 version:((\d){1,2}|(\.)){7}\|((\d){1,2}|(\.)){7}"},
            { Component.Driver, @"# driver64 version:((\d){1,2}|(\.)){7}\|((\d){1,2}|(\.)){7}"},
            { Component.GameShield, @"# gameshield64 version:((\d){1,2}|(\.)){7}\|((\d){1,2}|(\.)){7}"},
            { Component.Launcher, @"# launcher64 version:((\d){1,2}|(\.)){7}\|((\d){1,2}|(\.)){7}" },
            { Component.Updater, @"# frostupdater64 version:((\d){1,2}|(\.)){7}\|((\d){1,2}|(\.)){7}" }
        };

        public static string Parse(string rawData, Component componentType)
        {
            string preparedMessage = PreparationForParsing(rawData);
            StringBuilder resultBuilder = new StringBuilder();

            foreach (Component comp in valuableComponents)
            {
                if ((componentType & comp) == comp)
                {
                    if (get32BitComponents)
                    {
                        Regex resultsParser = new Regex(componentPatterns32[comp]);
                        foreach (Match match in resultsParser.Matches(preparedMessage))
                        {
                            resultBuilder.Append(match);
                            resultBuilder.Append(Environment.NewLine);
                        }
                    }

                    if (get64BitComponents)
                    {
                        Regex resultsParser = new Regex(componentPatterns64[comp]);
                        foreach (Match match in resultsParser.Matches(preparedMessage))
                        {
                            resultBuilder.Append(match);
                            resultBuilder.Append(Environment.NewLine);
                        }
                    }
                }
            }
            
            return resultBuilder
                .Replace(":", Environment.NewLine)
                .Replace("|", Environment.NewLine)
                .ToString();
        }

        private static string PreparationForParsing(string input)
        {
            StringBuilder innerString = new StringBuilder();
            using (StringReader readFromInput = new StringReader(input))
            {
                while (readFromInput.Peek() != -1)
                {
                    string nextLine = String.Empty;
                    while (nextLine.Length < 1) nextLine = readFromInput.ReadLine();
                    if (nextLine[0] == PARAMS_DELIM)
                    {
                        innerString.AppendLine(nextLine);
                    }

                    else break;
                }
            }

            return innerString.ToString();
        }
    }
}