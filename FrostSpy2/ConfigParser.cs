﻿namespace FrostSpy2
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Linq;
    using System.Xml;


    public static class ConfigParser
    {
        private static Dictionary<string, string> environments;
        private static List<string> connectionData;
        private static List<Game> games;

        public static Dictionary<string, string> GetParsedEnvironments
        {
            get { return environments; }
        }
        public static List<string> GetParsedConnections
        {
            get { return connectionData; }
        }
        public static List<Game> GetParsedGames
        {
            get { return games; }
        }

        public static void Parse(string fileName)
        {
            XmlDocument xDoc = new XmlDocument();
            xDoc.Load(fileName);
            XmlElement xRoot = xDoc.DocumentElement;

            XmlNode envsNode = xRoot.SelectSingleNode("environments");
            environments = GetEnvironments(envsNode);

            XmlNode connectionNode = xRoot.SelectSingleNode("connectionData");
            connectionData = GetTextDataFromNodeChildren(connectionNode, new List<string>() { "dns", "ip" });

            XmlNode gamesNode = xRoot.SelectSingleNode("gamesCollection");
            games = ParseGameData(gamesNode);
        }

        private static Dictionary<string, string> GetEnvironments(XmlNode environmentsNode)
        {
            XmlNodeList childNodes = environmentsNode.ChildNodes;
            var parsedEnvs = new Dictionary<string, string>();

            foreach (XmlNode childNode in childNodes)
            {
                if (childNode.Name != "env" ||
                    !childNode.HasChildNodes)
                {
                    throw new ArgumentException("wrong data in ipCollection!");
                }

                string envName = childNode.Attributes?["name"].Value;
                string envConnectionString = childNode.InnerText.Trim();

                if (string.IsNullOrEmpty(envName) || string.IsNullOrEmpty(envConnectionString))
                {
                    throw new DataException($"wrong environment data in name or connection string value!");
                }

                parsedEnvs.Add(envName, envConnectionString);
            }

            return parsedEnvs;
        }

        private static List<Game> ParseGameData(XmlNode nodeWithGames)
        {
            XmlNodeList childNodes = nodeWithGames.ChildNodes;

            List<Game> parsedGames = new List<Game>();

            foreach (XmlNode childNode in childNodes)
            {
                if (childNode.Name != "game")
                {
                    throw new ArgumentException("wrong data in ipCollection!");
                }

                Game game = new Game();

                if (childNode.Attributes != null)
                {
                    foreach (XmlAttribute attr in childNode.Attributes)
                    {
                        if (attr.Name == "name")
                        {
                            if (parsedGames.Exists(gm => gm.Name == attr.Value))
                                throw new DataException("two games with same name detected in config file! game name must be unique"); 
                            else game.Name = attr.Value;
                        }
                        else
                        {
                            game.ParamsCollection.Add(attr.Name, attr.Value);
                        }
                    }
                }

                XmlNodeList overrideUrlNodes = childNode.ChildNodes;

                foreach (XmlNode overrideNode in overrideUrlNodes)
                {
                    string envName = overrideNode.Attributes?["env"].Value;
                    if (string.IsNullOrEmpty(envName))
                        throw new DataException("Environment name problem in override url element");

                    if (environments?.ContainsKey(envName) == true)
                    {
                        game.OverrideRules.Add(envName, overrideNode.InnerText.Trim());
                    }
                }

                parsedGames.Add(game);
            }

            return parsedGames;
        }

        private static List<string> GetTextDataFromNodeChildren(XmlNode node, IReadOnlyCollection<string> possibleElements)
        {
            List<string> resultList = new List<string>();

            XmlNodeList childNodes = node.ChildNodes;
            foreach (XmlNode childNode in childNodes)
            {
                if (!possibleElements.Any(str => str == childNode.Name) ||
                    !childNode.HasChildNodes)
                {
                    throw new ArgumentException("wrong data in ipCollection!");
                }

                string rst = childNode.InnerText.Trim();
                Console.WriteLine(rst);
                resultList.Add(rst);
            }

            return resultList;
        }
    }
}
